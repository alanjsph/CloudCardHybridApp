angular.module('starter.controllers')
.controller('CardsCtrl', function($scope, $ionicModal, MyConnectionsService, CategoryService, $ionicScrollDelegate) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  //--------------- Select Category Modal
  $ionicModal.fromTemplateUrl('components/cards/modals/modal-select-category.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.chooseCategoryModel = modal;
  });
  $scope.openChooseCategoryModal = function() {
    $scope.chooseCategoryModel.show();
  };
  $scope.closeChooseCategoryModal = function() {
    $scope.chooseCategoryModel.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.chooseCategoryModel.remove();
  });
  // Execute action on hide modal
  $scope.$on('chooseCategoryModel.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('chooseCategoryModel.removed', function() {
    // Execute action
  });


  //--------------- Select view Modal
  $ionicModal.fromTemplateUrl('components/cards/modals/modal-select-view.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.chooseViewModal = modal;
  });
  $scope.openChooseViewModal = function() {
    $scope.chooseViewModal.show();
  };
  $scope.closeChooseViewModal = function() {
    $scope.chooseViewModal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.chooseViewModal.remove();
  });
  // Execute action on hide modal
  $scope.$on('chooseViewModal.hidden', function() {
    // Execute action

  });
  // Execute action on remove modal
  $scope.$on('chooseViewModal.removed', function() {
    // Execute action
  });

  //--------------------------------------------

  //--------------- Add category  modal
  $ionicModal.fromTemplateUrl('components/cards/modals/modal-addCategory.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.addCategoryModal = modal;
  });
  $scope.openAddCategoryModal = function() {
    $scope.addCategoryModal.show();
  };
  $scope.closeAddCategoryModal = function() {
    $scope.addCategoryModal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.addCategoryModal.remove();
  });
  // Execute action on hide modal
  $scope.$on('addCategoryModal.hidden', function() {
    // Execute action

  });
  // Execute action on remove modal
  $scope.$on('addCategoryModal.removed', function() {
    // Execute action

  });

  //--------------------------------------------
  $scope.activeCategory = "all";
  $scope.activeCategoryDescription = "List of all of your business cards.";
  $scope.activeCategoryName = "All";
  $scope.myConnections = MyConnectionsService.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
  $scope.myCategories = CategoryService.all();

  $scope.updateCategoryFullById = function(categoryId){
    if(categoryId == "all"){
      $scope.myConnections = MyConnectionsService.all();
      $scope.activeCategoryDescription = "List of all of your business cards.";
      $scope.activeCategoryName = "All";
    }
    else{
      $scope.myConnections = MyConnectionsService.getByCategoryId(categoryId);
      $scope.activeCategoryDescription = CategoryService.getCategoryDescription(categoryId);
      $scope.activeCategoryName = CategoryService.getCategoryName(categoryId);
    }
    $ionicScrollDelegate.scrollTop();
    }






});
