angular.module('starter.controllers')
.controller('CardDetailCtrl', function($scope, $stateParams, MyConnectionsService, $ionicLoading) {
  $scope.showLoading = function() {
    $ionicLoading.show({
      template: 'Just a moment...'
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };
  $scope.hideLoader = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };
  $scope.showLoading();

  $scope.card = MyConnectionsService.get($stateParams.cardId);





});
