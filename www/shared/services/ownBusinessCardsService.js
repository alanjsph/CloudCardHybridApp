angular.module('starter.services')

//------ the business card DB
.factory('OwnBusinessCards', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var ownbusinesscards = [
  {
    "id": "57b0d74dc661a9035c586895",
    "index": 0,
    "cardName": "Stranger",
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "male",
    "email": "nanniehartman@cedward.com",
    "registered": "2014-01-18T10:15:19 -06:-30"
  },
  {
    "id": "57b0d74d8fef0f95692af8cc",
    "index": 1,
    "cardName": "Social Media Only",
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "female",
    "facebook": "alanjsph",
    "twitter": "alanjsph",
    "linkedIn": "alanjsph",
    "registered": "2014-12-27T05:10:47 -06:-30"
  },
  {
    "id": "57b0d74df9412ffef8b19713",
    "index": 2,
    "cardName": "Work",
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "male",
    "company": "ECOSYS",
    "email": "lambnoel@ecosys.com",
    "phone": "+1 (838) 440-3959",
    "registered": "2015-10-04T10:18:42 -06:-30"
  },
  {
    "id": "57b0d74d2096b0e519330490",
    "index": 3,
    "cardName": "Family",
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "male",
    "company": "CORIANDER",
    "email": "normanhill@coriander.com",
    "phone": "+1 (982) 517-2776",
    "address": "361 Harwood Place, Bentley, Oklahoma, 4426",
    "registered": "2016-04-19T06:02:00 -06:-30"
  }

];

  return {
    all: function() {
      return ownbusinesscards;
    },
    get: function(cardId) {
      for (var i = 0; i < ownbusinesscards.length; i++) {
        if (ownbusinesscards[i].id === cardId) {
          return ownbusinesscards[i];
        }
      }
      return null;
    }
  };
});
