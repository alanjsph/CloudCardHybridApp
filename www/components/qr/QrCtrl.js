
angular.module('starter.controllers')

.controller('QrCtrl', function($scope) {

$scope.scanQr = function(){
  cordova.plugins.barcodeScanner.scan(
      function (result) {
          alert("We got a barcode\n" +
                "Result: " + result.text + "\n" +
                "Format: " + result.format + "\n" +
                "Cancelled: " + result.cancelled);
      },
      function (error) {
          alert("Scanning failed: " + error);
      },
      {
        //  "preferFrontCamera" : true // iOS and Android
        //  "showFlipCameraButton" : true, // iOS and Android
          "prompt" : "Place the businesscard Qr inside the box", // supported on Android only
          "formats" : "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
          "orientation" : "portrait" // Android only (portrait|landscape), default unset so it rotates with the device
      }
   );
};


$scope.generateQr = function(qrData,size){    // use   <img ng-src="{{generateQr('alan','200')}}">
  return "https://chart.googleapis.com/chart?cht=qr&chl=" + qrData + "&chs=" + size + "x" + size + "&chld=Q|0";
};



});
