angular.module('starter.services')

//------ the business card DB
.factory('CategoryService', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var categories = [
  {
    "id": "57b0d74dc661a9035c586895",
    "name": "Strangers",
    "type": "general",
    "description": "This category contains business cards of strangers.",
    "defaultNote": "Default note for strangers",
    "color" : "red",
    "CreationDate": "2014-01-18T10:15:19 -06:-30"
  },
  {
    "id": "57b0d74d8fef0f95692af8cc",
    "name": "Family",
    "type": "general",
    "description": "All my family business cards",
    "defaultNote": "default note for family",
    "color" : "green",
    "CreationDate": "2014-12-27T05:10:47 -06:-30"
  },
  {
    "id": "57b0d74df9412ffef8b19713",
    "name": "The Kitchen Nov 2016",
    "type": "event",
    "description": "the entrepreneurs i met. The event venue is crowne plaza",
    "defaultNote": "default note for the kitchen entrepreneurs",
    "color" : "blue",
    "CreationDate": "2015-10-04T10:18:42 -06:-30"
  },
  {
    "id": "57b0d74d2096b0e519330490",
    "name": "Friends",
    "type": "general",
    "description": "All my friends's business cards are here.",
    "defaultNote": "default note for new friend connections",
    "color" : "white",
    "CreationDate": "2016-04-19T06:02:00 -06:-30"
  }

];

  return {
    all: function() {
      return categories;
    },
    getCategoryDescription: function(categoryId) {
      for (var i = 0; i < categories.length; i++) {
        if (categories[i].id === categoryId) {
          return categories[i].description;
          break;
        }
      }
    },
    getCategoryName: function(categoryId) {
      for (var i = 0; i < categories.length; i++) {
        if (categories[i].id === categoryId) {
          return categories[i].name;
          break;
        }
      }
    },
    get: function(categoryId) {
      for (var i = 0; i < categories.length; i++) {
        if (categories[i].id === categoryId) {
          return categories[i];
        }
      }
      return null;
    }
  };
});
