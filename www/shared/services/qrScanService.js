angular.module('starter.services')

//------ the business card DB
.factory('QrScanService', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var qrCards = [
  {
    "id": "57b0d74dc661a9035c586895",
    "index": 0,
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "male",
    "email": "nanniehartman@cedward.com"
  },
  {
    "id": "57b0d74d8fef0f95692af8cc",
    "index": 1,
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "female",
    "facebook": "alanjsph",
    "twitter": "alanjsph",
    "linkedIn": "alanjsph"
  },
  {
    "id": "57b0d74df9412ffef8b19713",
    "index": 2,
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "male",
    "company": "ECOSYS",
    "email": "lambnoel@ecosys.com",
    "phone": "+1 (838) 440-3959"
  },
  {
    "id": "57b0d74d2096b0e519330490",
    "index": 3,
    "picture": "http://lorempixel.com/g/600/600/",
    "name": "Alan Joseph",
    "gender": "male",
    "company": "CORIANDER",
    "email": "normanhill@coriander.com",
    "phone": "+1 (982) 517-2776",
    "address": "361 Harwood Place, Bentley, Oklahoma, 4426"
  }

];

  return {
    getBusinessCardByQr: function(cardID) {
      for (var i = 0; i < qrCards.length; i++) {
        if (qrCards[i].id === cardId) {
          return qrCards[i];
        }
      }
      return null;
    }
  };
});
