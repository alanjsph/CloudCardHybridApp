// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordovaOauth', 'ionicLazyLoad'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(['$ionicConfigProvider', function($ionicConfigProvider) {   // To force place the tabs to bottom on all platforms

    $ionicConfigProvider.tabs.position('bottom'); // other values: top

}])

// directive to call fucntion on image load

.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                scope.hideLoader();
            });
            element.bind('error', function(){
                scope.hideLoader();
                alert('image could not be loaded');
            });
        }
    };
})




.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'components/tabs/tabsView.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.notifications', {
    url: '/notifications',
    views: {
      'tab-notifications': {
        templateUrl: 'components/notifications/notificationsView.html',
        controller: 'NotificationsCtrl'
      }
    }
  })

  .state('tab.nearby', {
      url: '/nearby',
      views: {
        'tab-nearby': {
          templateUrl: 'components/nearby/nearbyView.html',
          controller: 'NearbyCtrl'
        }
      }
    })

  .state('tab.cards', {
      url: '/cards',
      views: {
        'tab-cards': {
          templateUrl: 'components/cards/cardsView.html',
          controller: 'CardsCtrl'
        }
      }
    })
    .state('tab.card-detail', {
      url: '/cards/:cardId',
      views: {
        'tab-cards': {
          templateUrl: 'components/cards/cardDetails/cardDetailsView.html',
          controller: 'CardDetailCtrl'
        }
      }
    })

    .state('tab.qr', {
        url: '/qr',
        views: {
          'tab-qr': {
            templateUrl: 'components/qr/qrView.html',
            controller: 'QrCtrl'
          }
        }
      })

  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'components/settings/settingsView.html',
        controller: 'SettingsCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/notifications');

});
